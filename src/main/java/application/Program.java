package application;

import entities.ImportedProduct;
import entities.Product;
import entities.UsedProduct;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        List<Product> productList = new ArrayList<>();

        System.out.print("Enter the number of products: ");
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.printf("Product #%d data: ", i + 1);
            System.out.print("\nCommon, used or imported (c/u/i)? ");
            char answer = sc.next().charAt(0);
            sc.nextLine();
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Price: ");
            Double price = sc.nextDouble();

            if (answer == 'i') {
                System.out.print("Customs fee: ");
                Double customFee = sc.nextDouble();
                Product product = new ImportedProduct(name, price, customFee);
                productList.add(product);
            } else if (answer == 'u') {
                System.out.print("Manufacture date (DD/MM/YYYY): ");
                LocalDate date = LocalDate.parse(sc.next(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                Product product = new UsedProduct(name, price, date);
                productList.add(product);
            } else {
                Product product = new Product(name, price);
                productList.add(product);
            }
            System.out.println();
        }
        System.out.println("PRICE TAGS: ");
        for (Product product : productList){
            System.out.println(product.priceTag());
        }
    }
}

//LocalDate date = LocalDate.parse(sc.next(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
