import entities.ImportedProduct;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class ImportedProductTest {

    private ImportedProduct importedProduct;

    @BeforeEach
    public void initiateTest() {
        this.importedProduct = new ImportedProduct("iPhone", 400.00, 20.00);
    }

    @Test
    public void shouldReturnTotalPriceOfImportedProduct() {
        double total = importedProduct.totalPrice();
        assertEquals(420, total);
    }

    @Test
    public void shouldNotReturnIncorrectTotalPrice(){
        assertNotEquals(500, importedProduct.totalPrice());
    }

}
